// var randomName = {
//   0: "海豹", 1:"水牛", 2:"獼猴", 3:"狐狸", 4:"河馬", 
//   5: "麋鹿", 6:"野狼", 7:"狸貓", 8:"章魚", 9:"熊貓"
// }

const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

// exports.newUserAdded = functions.database.ref('/userData/{uid}/email')
//     .onCreate((snapshot, context) => {
//         const userDataRef = snapshot.ref.parent;
//         userDataRef.child("nickname").once('value').then(sp=>{
//           if(!sp.exists()){
//             const uid = userDataRef.key;
//             const randSet = uid[uid.length - 1].charCodeAt();
//             let name;
//             if(randSet >= 48 && randSet <= 57){
//               name = "間諜"+randomName[randSet-48];
//             } else if (randSet >= 65 && randSet <= 90){
//               name = "間諜"+randomName[(randSet-65)%10];
//             } else if (randSet >= 97 && randSet <= 122){
//               name = "間諜"+randomName[(randSet-97)%10];
//             }
//             userDataRef.update({nickname: name});
//           }
          
//         })
//         userDataRef.child('room/0000').set(0);
//     });

exports.newUserAddChatroom = functions.database.ref('/chatRoom/{roomid}/member/{uid}')
    .onCreate((snapshot, context) => {
        const uid = snapshot.key;
        admin.database().ref('userData/'+uid+'/nickname').once('value').then(sp => {
          snapshot.ref.parent.parent.child('message').push({
            "nickname": sp.val(),
            "mode": "info"
          })
        })
    });