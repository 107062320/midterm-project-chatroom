# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : ChatRoom
* Key functions 
    1. listenRoom
        * 監聽當前聊天室是否有新訊息
    2. createMessageTag
        * 將訊息結構轉換成dom元素
    3. setRoomList
        * 顯示使用者擁有的聊天室並持續監聽
    4. createChatRoomTag
        * 將使用者聊天室結構轉換成dom元素
    5. changeRoom
        * 切換房間，更新畫面
    6. sendMessage
        * 寄送訊息
    7. createChatRoom
        * 新建房間
    8. joinChatRoom
        * 加入房間
    9. addMember
        * 新增成員
    10. buildNoti
        * 確認通知狀態
 
    
* Other functions
    1. cdSecretMes
        * 限時訊息倒數計時 
    

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Secret Message|1~10%|Y|

## Website Detail Description

使用者若在未登入狀態下，會被移往登入頁
此時可登入既有帳號或選用Google帳號登入
![](https://i.imgur.com/gHHtrta.png)

使用者亦可使用上方的頁籤來切換成註冊頁面
表單的每一項具有簡易的檢查功能
![](https://i.imgur.com/LOe2KOD.png)

登入後會移回主頁面
左側顯示個人資訊、擁有的聊天室以及功能按鈕
右側則為聊天室本體
![](https://i.imgur.com/ixyPxSR.png)

### 功能按鈕介紹
下方的"新增"按鈕按下後，會詢問新聊天室的名稱
點擊確認後即可在左方聊天室欄選取進入
![](https://i.imgur.com/yxrwQOP.gif)

下方的"加入"按鈕按下後，會詢問欲加入的聊天室ID
若此聊天室存在將會在左方聊天室顯示
![](https://i.imgur.com/dNUxTMM.gif)

下方的"登出"按鈕按下後，即會登出，並轉跳至登入頁
![](https://i.imgur.com/WPtowlt.gif)

### 聊天室介面

右上方的"新增成員"按鈕按下後，將會提示輸入email
若該使用者存在，將會把使用者加入此聊天室
![](https://i.imgur.com/sD2b3xp.gif)

在下方輸入訊息並送出後，對方會同步看到訊息
![](https://i.imgur.com/949uH26.gif)

*進階功能*
若開啟限時模式，對方收到後將開始倒數
時限結束後將會銷毀訊息
![](https://i.imgur.com/bqFm90P.gif)

### 其他功能
當視窗**未作用**時，將會把當前聊天室的訊息以通知送出
![](https://i.imgur.com/GkjhHqV.gif)

對各類螢幕尺寸做RWD優化
![](https://i.imgur.com/Y5JzrSh.gif)

封鎖html語法使用
![](https://i.imgur.com/sNDg36n.png)


# 作品網址：
https://chatroom-107062320.web.app/

# Components Description : 
1. Membership Mechanism :
    透過firebase提供的Authentication服務建立帳號及登入
    並在database內存放各會員的資料
2. Firebase Page : 
    使用firebase提供的hosting服務，建立可供連線的網頁
3. Database : 
    使用firebase提供的database服務完成下列主要功能：
    1. 紀錄各個使用者的個人資料，如暱稱、email、擁有的房間...等
    2. 紀錄各個聊天室的名稱、會員、歷史訊息...等
    3. 提供email->uid的查詢，降低會員其他資料的曝光
    同時建立規則避免資料遭到破壞
4. RWD
    透過css的@media語法，在不同螢幕尺寸間更改大小、排列方式...等
5. Topic Key Function
    * 新建房間
    向使用者詢問房名後，尋找可使用的ID並建立房間
    在該房間註冊此使用者，並在該使用者所擁房間新增此房間
    * 傳送訊息
    使用者觸發傳送後，將訊息資料打包後送至database對應位置
    * 載入歷史訊息
    從database對應位置載入訊息後顯示至畫面
6. Third-Party Sign In
    透過firebase提供的Authentication服務登入第三方帳號
7. Chrome Notification
    使用Notification這個特殊Class向瀏覽器要求權限
    並在視窗blur時推送通知
8. Use CSS Animation
    在Login畫面中環繞視窗的邊框即為CSS動畫
    此動畫運用了偽元素before, after來產生邊框
    並透過clip-path的變化產生邊框在移動的景象
    最後運用animation-delay產生對應的感覺
![](https://i.imgur.com/EX5wqT2.gif)

# Other Functions Description : 
1. cdSecretMes
    此function是用來配合createMessageTag使用
    當createMessageTag創造一個須倒數的訊息後
    此function將會呼叫多個setTimeout函式
    以期在指定時間變更顯示文字
    並搭配CSS達到在畫面上銷毀的效果
    
2. newUserAddChatroom(@cloud functions)
    此function用以監聽聊天室有人加入後
    在聊天訊息內新增通知

## Security Report

### Database
在userData內，存放各個使用者的資料
僅有使用者的uid相符時才可更動及觀看
但各使用者所擁有的房間可被其他使用者新增

在chatRoom內，存放各個聊天室的資料
若在公開房(0000)，僅有新的訊息可被寫入(即不可更動或刪除)
若已讀某限時訊息，僅可在該訊息的"read"區塊填入自身的uid

若在私人房(即其他房間)，
房間成員僅能被成員加入或自身加入，
訊息除了有公開房的限制外還須為房間成員，
房間名亦僅能被成員修改

在email2uid內，存放{email: id}的結構
開放所有人閱覽，但一旦寫入就會轉為唯獨

### HTML message
在建立各個動態元件時(如聊天訊息、聊天室)，皆採用createElement的方式
並且將使用者輸入的文本用innerText填入
避免被瀏覽器辨識為DOM元件


## 外部資源
### 登入
#### 套用模板
https://codepen.io/ehermanson/pen/KwKWEv
* 修改後的prototype(Pug + Scss)
    https://codepen.io/jokerljay/pen/jObyLGr

### 聊天室本體
#### 參考設計稿
https://bootsnipp.com/snippets/exR5v
* prototype(Pug + Sass)
    https://codepen.io/jokerljay/pen/WNQpgzb
    
### 服務
* Firebase
* Font Awesome
* Validator (for bootstrap 3)
* jQuery
* Bootstrap