var roomIndex;
var first_count = 0;
var second_count = 0;
var room_count = 0;
var room_second_count = 0;
var userDataPack;
var notiPermi;
var sendMode = "normal";
var test;

function initApp(){
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      buildNoti();
      setUserInfo(user);
      setRoomList(user);
      changeRoom("0000");
    } else {
      document.location.href="./login.html";
    }
  });

  setFunctionBtn();
}

function setFunctionBtn(){
  document.getElementById('btnCreateChatRoom').addEventListener('click', ()=>{
    createChatRoom();
  });
  document.getElementById('btnJoinChatRoom').addEventListener('click', ()=>{
    joinChatRoom();
  });
  document.getElementById('btnLogOut').addEventListener('click', ()=>{
    firebase.auth().signOut();
  });
  document.getElementById('btnAddMember').addEventListener('click', ()=>{
    addMember();
  });
  document.getElementById('btnSecret').addEventListener('click', ()=>{
    changeSendMode();
  })
  document.getElementById('btnSend').addEventListener('click', ()=>{
    sendMessage();
  })
  document.getElementById('textInput').addEventListener('keypress', (e)=>{
    if(e.key != "Enter") return;
    sendMessage();
  })
}

function setUserInfo(user){
  let uid = user.uid;
  let userDataRef = firebase.database().ref("userData/"+uid);
  userDataRef.once('value').then(function(userData){
    let tagEmail = document.createElement("div");
    let tagNickname = document.createElement("div");
    tagEmail.className = "userEmail";
    tagNickname.className = "userNickName";
    tagEmail.innerText = userData.val().email;
    tagNickname.innerText = userData.val().nickname;
    tagNickname.appendChild(tagEmail);
    userDataPack = {
      uid: uid,
      nickname: userData.val().nickname,
      email: user.email,
      dataRef: userDataRef
    }
    document.getElementsByClassName('userNickName')[0].innerHTML = tagNickname.innerHTML;
  })
}

function listenRoom(id){
  const roomRef = firebase.database().ref('chatRoom/'+id+"/message");
  roomRef.on('child_added', function(data){
    if(id !== roomIndex) return;
    second_count++;
    if(second_count <= first_count) return;
    const childData = data.val();
    const element = document.getElementById('messageList');
    const atBottom = (element.scrollTop >= element.scrollHeight - element.clientHeight);
    createMessageTag(childData, data.ref, data.key);
    let notifyConfig = {
      body: childData.text,
      tag: id
    };
    const roomName = $('#chatName').text();
    if(!document.hasFocus() && childData.email != userDataPack.email){
      if(childData.type != "info"){
        let notification = new Notification(childData.nickname+'@'+roomName, notifyConfig);
      }
      
    }
    if(atBottom) gotoBottom();
  })
}

function createMessageTag(data, ref=null, key=null){
  let email = data.email, text = data.text, nickname = data.nickname, mode = data.mode;
  if(mode == "info"){
    createInfoTag(data, key);
    return;
  }
  const userEmail = firebase.auth().currentUser.email;
  let type;
  if(userEmail == email) type = "sent";
  else type = "replies";
  let tagli = document.createElement("li");
  let tagdivP = document.createElement("div");
  let tagdivM = document.createElement("div");
  let tagdivN = document.createElement("div");
  let tagdivT = document.createElement("div");
  let tagdivS = document.createElement("div");
  let tagspan = document.createElement("span");
  let needCD = false;
  tagli.className = type;
  tagdivP.className = "talkPic";
  tagdivM.className = "talkMes";
  tagdivN.className = "talkName";
  tagdivT.className = "talkText";
  tagdivS.className = "secretNot";
  tagdivT.innerText = text;
  tagdivN.innerText = nickname;
  tagdivM.appendChild(tagdivN);
  tagdivM.appendChild(tagdivT);
  if(mode == "secret"){
    if(type == "replies") {
      try {
        if(data.read[userDataPack.uid] === true) {
          tagspan.innerText = "已過期";
          tagdivT.className += " outdated";
        } else {
          ref.child('read/'+userDataPack.uid).set(true);
          needCD = true;
        }
      } catch(e) {
        ref.child('read/'+userDataPack.uid).set(true);
        needCD = true;
      }
    }
    tagdivS.innerText = "限時訊息 ";
    
    tagdivS.appendChild(tagspan);
    tagdivM.appendChild(tagdivS);
  }
  tagli.appendChild(tagdivP);
  tagli.appendChild(tagdivM);
  tagli.id = key;
  document.getElementById('messageList').appendChild(tagli);
  if(needCD) cdSecretMes(key);
}

function createInfoTag(data, key){
  let tagli = document.createElement("li");
  let tagdivM = document.createElement("div");
  let tagdivT = document.createElement("div");
  tagli.className = "info";
  tagdivM.className = "talkMes";
  tagdivT.className = "talkText";
  tagdivT.innerText = data.nickname+" 加入聊天室";
  tagdivM.appendChild(tagdivT);
  tagli.appendChild(tagdivM);
  tagli.id = key;
  document.getElementById('messageList').appendChild(tagli);
}

function cdSecretMes(key){
  $('#'+key+' span').text("00:05");
  for(var i = 1; i <= 5; i++){
    if(i == 5){
      setTimeout(function(x){
        $('#'+key+' span').text("已過期");
        $('#'+key+' .talkText').addClass('outdated');
      }, i*1000, i)
    } else {
      setTimeout(function(x){
        $('#'+key+' span').text("00:0"+(5-x));
      }, i*1000, i)
    }
  }
}

function setRoomList(user){
  const userRoomRef = firebase.database().ref('userData/'+user.uid+"/room");
  userRoomRef.orderByValue().once('value').then(function(snapshot){
    snapshot.forEach(function(item){
      createChatRoomTag(item.key);
      room_count++;
    });
    userRoomRef.on('child_added', function(data){
      room_second_count++;
      if(room_second_count <= room_count) return;
      createChatRoomTag(data.key, true);
    })
  }
  )
}

function createChatRoomTag(index, newadded=false){
  const roomNameRef = firebase.database().ref('chatRoom/'+index+"/roomName");
  roomNameRef.once('value').then(function(item){
    let roomName = item.val();
    let tagli = document.createElement("li");
    let tagdivP = document.createElement("div");
    let tagdivN = document.createElement("div");
    tagdivP.className = "chatPic";
    tagdivN.className = "chatName";
    tagdivN.innerText = roomName;
    tagli.id = index;
    tagli.appendChild(tagdivP);
    tagli.appendChild(tagdivN);
    tagli.addEventListener('click', function(){
      changeRoom(index);
    });
    document.getElementById('chatList').appendChild(tagli);
    if(newadded){
      $('#'+index).addClass("newadded");
      setTimeout(function(x){
        $('#'+x).removeClass("newadded");
      }, 3000, index);
    }
  }).then(()=>{
    $('#'+roomIndex).addClass('active');
  })
}

function changeRoom(nindex){
  console.log(nindex);
  console.log("changeRoom");
  if(nindex === roomIndex) return;
  first_count = second_count = 0;
  if(roomIndex !== undefined){
    $('#'+roomIndex).removeClass("active");
    const oldRef = firebase.database().ref('chatRoom/'+roomIndex+"/message");
    oldRef.off();
  }
  document.getElementById('messageList').innerHTML = "";
  roomIndex = nindex;
  const newMesRef = firebase.database().ref('chatRoom/'+nindex+"/message");
  const roomNameRef = firebase.database().ref('chatRoom/'+nindex+"/roomName");
  roomNameRef.once('value').then(function(item){
    $('#chatName').text(item.val());
    $('#chatId').text('#'+nindex);
    if(nindex == "0000"){
      $('#btnAddMember').css("display", "none");
    } else {
      $('#btnAddMember').css("display", "inline-block");
    }
  });
  newMesRef.once('value').then(function(snapshot){
    snapshot.forEach(function(item){
      let data = item.val();
      createMessageTag(data, item.ref, item.key);
      first_count++;
    });
    listenRoom(nindex);
    gotoBottom();
  })
  $('#'+nindex).addClass("active");
}

function sendMessage(){
  const text = document.getElementById('textInput').value;
  if(text === "") return;
  const roomRef = firebase.database().ref('chatRoom/'+roomIndex+"/message");
  roomRef.push({
    "email": userDataPack.email,
    "nickname": userDataPack.nickname,
    "text": text,
    "mode": sendMode
  })
  document.getElementById('textInput').value = "";
  gotoBottom();
}

function gotoBottom(){
  let element = document.getElementById('messageList');
  element.scrollTop = element.scrollHeight - element.clientHeight;
}

function createChatRoom(){
  let roomName = prompt("新聊天室名稱：", userDataPack.nickname+"'s Chat Room");
  if(roomName == null){
    return;
  } else if (roomName == ""){
    alert("聊天室名需至少一個字");
    return;
  }
  const chatRoomRef = firebase.database().ref('chatRoom');
  function paddingLeft(str){
    if(str.length >= 4)
    return str;
    else
    return paddingLeft("0" +str);
  }
  chatRoomRef.once('value').then((snapshot)=>{
    let newRoomId = Math.floor(Math.random() * 10000).toString();
    newRoomId = paddingLeft(newRoomId);
    let found = false;
    while(!found){
      found = true;
      snapshot.forEach(function(item){
        if(newRoomId == item.key){
          found = false;
        }
      })
      if(!found){
        newRoomId = Math.floor(Math.random() * 10000);
      }
    }
    console.log(newRoomId);
    chatRoomRef.child(newRoomId).set({
      "roomName": roomName
    });
    chatRoomRef.child(newRoomId+'/member/'+userDataPack.uid).set(true);
    const time = new Date();
    userDataPack.dataRef.child("room/"+newRoomId).set(time.getTime());
  })
}

function joinChatRoom(){
  let roomId = prompt("聊天室ID：(eg. 0101)");
  if(roomId == null) return;
  let re = new RegExp("^[0-9]{4}$");
  if(re.exec(roomId) == null){
    alert("格式錯誤");
    return;
  }
  if(document.getElementById(roomId) != null){
    alert('你已為該房間成員');
    return;
  }
  const chatRoomRef = firebase.database().ref("chatRoom/"+roomId+"/roomName");
  chatRoomRef.once('value').then(function(snapshot){
    if(!snapshot.exists()){
      alert("聊天室不存在")
      return;
    }
    chatRoomRef.parent.child("member/"+userDataPack.uid).set(true);
    const time = new Date();
    userDataPack.dataRef.child("room/"+roomId).set(time.getTime());
  })
}

function addMember(){
  if(roomIndex == "0000") return;
  let user = prompt("新增成員email：");
  if(user == null) return;
  let re = /^([\w\.\-]){1,64}\@([\w\.\-]){1,64}$/;
  if(!re.test(user)) {
    alert("格式錯誤");
    return;
  } else if (user == userDataPack.email){
    alert("你已在此聊天室");
    return;
  }

  const userDataRef = firebase.database().ref("userData");
  const chatRoomRef = firebase.database().ref("chatRoom/"+roomIndex+"/member");
  const emailRef = firebase.database().ref("email2uid/"+email2key(user));
  emailRef.once('value').then(snapshot => {
    if(!snapshot.exists()){
      alert("該用戶不存在");
      return;
    }
    chatRoomRef.child(snapshot.val()).once('value').then(sp=>{
      if(sp.exists() || sp.val() == true){
        alert("該用戶已在此聊天室");
        return;
      }
      const time = new Date();
      sp.ref.set(true);
      userDataRef.child(sp.key+"/room/"+roomIndex).set(time.getTime());
      alert("成功加入該用戶");
    })
  })
}

function buildNoti(){
  if (!('Notification' in window)) {
    console.log('This browser does not support notification');
    return;
  }
  if (Notification.permission === 'default' || Notification.permission === 'undefined') {
    Notification.requestPermission(function(permission) {
      notiPermi = (permission == "granted") ? true : false;
    });
  }
}

function changeSendMode(){
  if(sendMode == "normal"){
    sendMode = "secret";
    $('#btnSecret').addClass('active');
  } else {
    sendMode = "normal";
    $('#btnSecret').removeClass('active');
  }
  
}



window.onload = function() {
  initApp();
}


function email2key(email){
  return email.replace(/[.]/g, '%20');
}