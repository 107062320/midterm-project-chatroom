var randomName = {
  0: "海豹", 1:"水牛", 2:"獼猴", 3:"狐狸", 4:"河馬", 
  5: "麋鹿", 6:"野狼", 7:"狸貓", 8:"章魚", 9:"熊貓"
}
$('.form').find('input, textarea').on('blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      label.addClass('active highlight');
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).find("#custom-alert-"+target.substr(1)).html("");
  $(target).find(".help-block").html("");
  $(target).find('input, textarea').val("");
  $(target).find("label").removeClass('highlight active');   
  $(target).fadeIn(600);
  
});


$('#signupForm').validator().on('submit', function (e) {
  
  
  if (!e.isDefaultPrevented()) {
    e.preventDefault();
    $('button[type="submit"]').addClass('disabled');
    let userInfo = {
      nickname: $('#inputNickname').val(),
      email: $('#inputEmail').val(),
      password: $('#inputPassword').val()
    }
    firebase.auth().createUserWithEmailAndPassword(
        userInfo.email, userInfo.password
    ).then(function(){
      //document.location.href="./index.html";
      let date = new Date();
      let now = date.getTime();
      const uid = firebase.auth().currentUser.uid;
      const database = firebase.database();
      database.ref("userData/"+uid).set({
        signup: now,
        email: userInfo.email,
        nickname: userInfo.nickname
      }).then(()=>{
        database.ref('email2uid/'+email2key(firebase.auth().currentUser.email)).set(firebase.auth().currentUser.uid);
      }).then(() => {
        
        database.ref("userData/"+uid+"/room/0000").set(0).then(function(){
          console.log("submit");
          create_alert("signup", "success", "userInfo created successfully");
          document.location.href="./index.html";
          $('#signupForm').find('input, textarea').val("");
        }).catch(err => {
          create_alert("signup", "error", err.message);
        })
      }).catch(err => {
        create_alert("signup", "error", err.message);
      });
    }).catch(function(error){
      create_alert("signup", "error", error.message);
    });
  }
  e.preventDefault();
})

$('#loginForm').validator().on('submit', function (e) {
  
  if (!e.isDefaultPrevented()) {
    e.preventDefault();
    let userInfo = {
      email: $('#inputLoginEmail').val(),
      password: $('#inputLoginPassword').val()
    }
    firebase.auth().signInWithEmailAndPassword(
        userInfo.email, userInfo.password
    ).then(function(){
      console.log("login");
      document.location.href="./index.html";
    }).catch(function(error){
      create_alert("login", "error", error.message);
    });
  }
  e.preventDefault();
})

$('#btnGoogle').on('click', function(e){
  e.preventDefault();
  var provider = new firebase.auth.GoogleAuthProvider();
  firebase.auth().signInWithPopup(provider).then(function(result){
    const uid = firebase.auth().currentUser.uid;
    const database = firebase.database();
    database.ref("userData/"+uid).once('value').then(snapshot=>{
      if(!snapshot.exists()){
        createGoogleAccount();
      } else {
        document.location.href="./index.html";
      }
    })
  }).catch(function(error){
      create_alert("login", "error", error.message);
  })
})

function create_alert(form, type, message) {
  var alertarea = document.getElementById('custom-alert-'+form);
  if (type == "success") {
      str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "</div>";
      alertarea.innerHTML = str_html;
  } else if (type == "error") {
      str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "</div>";
      alertarea.innerHTML = str_html;
  }
}

function createGoogleAccount(){
  let date = new Date();
  let now = date.getTime();
  const uid = firebase.auth().currentUser.uid;
  const database = firebase.database();
  let name;
  const randSet = uid[uid.length - 1].charCodeAt();
  if(randSet >= 48 && randSet <= 57){
    name = "間諜"+randomName[randSet-48];
  } else if (randSet >= 65 && randSet <= 90){
    name = "間諜"+randomName[(randSet-65)%10];
  } else if (randSet >= 97 && randSet <= 122){
    name = "間諜"+randomName[(randSet-97)%10];
  }
  database.ref("userData/"+uid).set({
    signup: now,
    email: firebase.auth().currentUser.email,
    nickname: name
  }).then(()=>{
    database.ref('email2uid/'+email2key(firebase.auth().currentUser.email)).set(firebase.auth().currentUser.uid);
  }).then(() => {
    
    database.ref("userData/"+uid+"/room/0000").set(0).then(function(){
      console.log("submit");
      create_alert("login", "success", "userInfo created successfully");
      document.location.href="./index.html";
    }).catch(err => {
      create_alert("login", "error", err.message);
    })
  }).catch(err => {
    create_alert("login", "error", err.message);
  });
}

function email2key(email){
  return email.replace(/[.]/g, '%20');
}